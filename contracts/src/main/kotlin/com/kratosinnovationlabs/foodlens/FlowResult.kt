package com.kratosinnovationlabs.foodlens

import net.corda.core.identity.AbstractParty
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.SignedTransaction

@CordaSerializable
data class FlowResult(val stx: SignedTransaction, val recipient: AbstractParty?)