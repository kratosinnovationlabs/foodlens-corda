package com.kratosinnovationlabs.foodlens.states

import com.kratosinnovationlabs.foodlens.contracts.ProductOperationContract
import com.kratosinnovationlabs.foodlens.models.ProductOperation
import com.kratosinnovationlabs.foodlens.schemas.ProductOperationSchemaV1
import net.corda.core.contracts.BelongsToContract
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable

@BelongsToContract(ProductOperationContract::class)
@CordaSerializable
data class ProductOperationState(val member: AnonymousParty, val admin: AnonymousParty, val product_operation: ProductOperation)
    : QueryableState {

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is ProductOperationSchemaV1 -> ProductOperationSchemaV1.PersistentProductOperationState(product_operation)
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(ProductOperationSchemaV1)

    override val participants: List<AbstractParty>
        get() = listOfNotNull(member, admin).asSequence().distinct().map { it }.toList()
}