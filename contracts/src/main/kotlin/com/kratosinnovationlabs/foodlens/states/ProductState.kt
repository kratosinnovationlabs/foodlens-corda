package com.kratosinnovationlabs.foodlens.states

import com.kratosinnovationlabs.foodlens.contracts.ProductContract
import com.kratosinnovationlabs.foodlens.models.Product
import com.kratosinnovationlabs.foodlens.schemas.ProductSchemaV1
import net.corda.core.contracts.BelongsToContract
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable

@BelongsToContract(ProductContract::class)
@CordaSerializable
data class ProductState(var owner: AnonymousParty, var creator: AnonymousParty, var admin: AnonymousParty, val product: Product) : QueryableState {

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is ProductSchemaV1 -> ProductSchemaV1.PersistentProductState(product)
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(ProductSchemaV1)

    override val participants: List<AbstractParty>
        get() = listOfNotNull(owner, creator, admin).asSequence().distinct().map { it }.toList()

    fun status(status: String) = copy(product = product.copy(status = status))
    fun transfer(owner: AnonymousParty, farmer: AnonymousParty = this.owner) = copy(owner = owner, creator = farmer)

}