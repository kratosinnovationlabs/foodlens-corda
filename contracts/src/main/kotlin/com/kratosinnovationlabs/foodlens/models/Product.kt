package com.kratosinnovationlabs.foodlens.models

import com.google.gson.annotations.SerializedName
import net.corda.core.serialization.CordaSerializable
import java.util.*

@CordaSerializable
data class Product(
        @SerializedName("product_id")
        var product_id: String,
        @SerializedName("started_at")
        var started_at: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("desc")
        var desc: String?,
        @SerializedName("product_name")
        var product_name: String,
        @SerializedName("done_by_id")
        var done_by_id: String
) {
    fun toOp() = ProductOperation(desc, null, "created", Date().toString(), product_id, done_by_id)
}

@CordaSerializable
data class ProductOperation(
        @SerializedName("desc")
        var desc: String?,

        @SerializedName("files")
        val files: List<String>?,

        @SerializedName("action_type")
        var action_type: String,

        @SerializedName("created_at")
        val created_at: String,

        @SerializedName("product_id")
        val product_id: String,

        @SerializedName("done_by_id")
        var done_by_id: String
)