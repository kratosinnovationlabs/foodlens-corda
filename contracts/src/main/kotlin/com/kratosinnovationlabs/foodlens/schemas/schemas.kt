package com.kratosinnovationlabs.foodlens.schemas

import com.kratosinnovationlabs.foodlens.models.Product
import com.kratosinnovationlabs.foodlens.models.ProductOperation
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.serialization.CordaSerializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

object ProductSchema

@CordaSerializable
object ProductSchemaV1 : MappedSchema(
        schemaFamily = ProductSchema.javaClass,
        version = 1,
        mappedTypes = listOf(PersistentProductState::class.java)
) {
    override val migrationResource = "product.changelog-master"

    @Entity
    @Table(
            name = "products",
            indexes = [
                Index(name = "product_product_idx", columnList = "product_id"),
                Index(name = "product_product_namex", columnList = "product_name")
            ]
    )
    class PersistentProductState(
            @Column(name = "product_id", length = 200, nullable = false)
            val product_id: String,

            @Column(name = "product_name", length = 200, nullable = false)
            val product_name: String,

            @Column(name = "started_at", length = 200, nullable = false)
            val started_at: String,

            @Column(name = "done_by_id", nullable = false)
            val done_by_id: String,

            @Column(name = "status", nullable = false)
            val status: String
    ) : PersistentState() {
        constructor(product: Product) : this(
                product_id = product.product_id,
                started_at = product.started_at,
                product_name = product.product_name,
                done_by_id = product.done_by_id,
                status = product.status
        )
    }
}


object ProductOperationSchema

@CordaSerializable
object ProductOperationSchemaV1 : MappedSchema(
        schemaFamily = ProductOperationSchema.javaClass,
        version = 1,
        mappedTypes = listOf(PersistentProductOperationState::class.java)
) {
    override val migrationResource = "product_operation.changelog-master"

    @Entity
    @Table(
            name = "product_operations",
            indexes = [
                Index(name = "product_operation_product_operation_idx", columnList = "product_id"),
                Index(name = "product_operation_product_operation_action_typex", columnList = "action_type")
            ]
    )
    class PersistentProductOperationState(

            @Column(name = "desc", length = 200, nullable = true)
            val desc: String?,

            @Column(name = "created_at", length = 200, nullable = false)
            val created_at: String,

            @Column(name = "action_type", nullable = false)
            val action_type: String,

            @Column(name = "product_id", nullable = false)
            val product_id: String,

            @Column(name = "done_by_id", nullable = false)
            val done_by_id: String
    ) : PersistentState() {
        constructor(product_operation: ProductOperation) : this(
                desc = product_operation.desc,
                created_at = product_operation.created_at,
                action_type = product_operation.action_type,
                product_id = product_operation.product_id,
                done_by_id = product_operation.done_by_id
        )
    }
}