package com.kratosinnovationlabs.foodlens.contracts

import com.kratosinnovationlabs.foodlens.states.ProductOperationState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction

class ProductOperationContract : Contract {
    companion object {
        const val CONTRACT_ID = "com.kratosinnovationlabs.foodlens.contracts.ProductOperationContract"
    }

    interface Commands : CommandData {
        class Add : Commands
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value) {
            is Commands.Add ->
                requireThat {
                    "There should be no input state." using (tx.inputs.isEmpty())
//                    "There should be two output states." using (tx.outputs.size == 2)
                    "One of the outputs should be of type ProductState." using (tx.outputs.filter { it.data is ProductOperationState }.size == 1)
                }
        }
    }
}