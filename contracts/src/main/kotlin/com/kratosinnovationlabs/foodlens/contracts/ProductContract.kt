package com.kratosinnovationlabs.foodlens.contracts

import com.kratosinnovationlabs.foodlens.states.ProductState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction

class ProductContract : Contract {
    companion object {
        const val CONTRACT_ID = "com.kratosinnovationlabs.foodlens.contracts.ProductContract"
    }

    interface Commands : CommandData {
        class Create : Commands
        class UpdateStatus : Commands
        class Close : Commands
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value) {
            is Commands.Create ->
                requireThat {
                    "There should be no input state." using (tx.inputs.isEmpty())
                    "There should be two output states." using (tx.outputs.size == 2)
                    "One of the outputs should be of type ProductState." using (tx.outputs.filter { it.data is ProductState }.size == 1)
                }
            is Commands.UpdateStatus -> requireThat {
                "There should be two input states." using (tx.inputs.size == 2)
            }
            is Commands.Close -> requireThat {
                "There should be one output state." using (tx.outputs.size == 1)
            }
        }
    }
}