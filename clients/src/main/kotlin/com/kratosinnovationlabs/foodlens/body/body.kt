package com.kratosinnovationlabs.foodlens.body

/**
 * File Name: [body]
 * Created By nareshkatta on 12-Apr-2022
 * Updated by Naresh Katta at 6:35 AM 12-Apr-2022
 */

data class JwtRequest(
    val username: String,
    val password: String
)

data class JwtResponse(val jwttoken: String)
