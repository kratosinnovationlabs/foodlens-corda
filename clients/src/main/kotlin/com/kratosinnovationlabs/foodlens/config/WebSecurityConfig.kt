package com.kratosinnovationlabs.foodlens.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

/**
 * File Name: [WebSecurityConfig]
 * Created By nareshkatta on 25-Mar-2022
 * Updated by Naresh Katta at 8:21 PM 25-Mar-2022
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableAutoConfiguration
@ComponentScan(value = ["com.kratosinnovationlabs.foodlens.config", "com.kratosinnovationlabs.foodlens.service"])
open class WebSecurityConfig constructor(
    val jwtAuthenticationEntryPoint: JwtAuthenticationEntryPoint,
    val jwtUserDetailsService: UserDetailsService,
    val jwtRequestFilter: JwtRequestFilter
) : WebSecurityConfigurerAdapter() {

    companion object {
        const val noSecure = true
    }

    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(NoOpPasswordEncoder.getInstance())
    }

    @Bean
    open fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        // We don't need CSRF for this example
        // We don't need CSRF for this example
        httpSecurity.csrf().disable()
            // dont authenticate this particular request
            .authorizeRequests().antMatchers(*patterns).permitAll().anyRequest()
            // all other requests need to be authenticated
            .authenticated().and().exceptionHandling()
            // make sure we use stateless session; session won't be used to
            // store user's state.
            .authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // Add a filter to validate the tokens with every request
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

    private val patterns =
        if (noSecure)
            arrayOf(
                "/error", "/authenticate", "/api/v1/me", "/api/v1/peers",
                "/api/v1/hash/**", "/api/v1/create_account/**",
                "/api/v1/add_product", "/api/v1/add_operation"
            )
        else
            arrayOf("/error", "/authenticate", "/api/v1/me", "/api/v1/peers")
}
