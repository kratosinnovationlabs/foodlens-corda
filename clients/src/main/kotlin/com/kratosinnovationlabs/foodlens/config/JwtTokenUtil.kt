package com.kratosinnovationlabs.foodlens.config

import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.io.Serializable
import java.util.*
import java.util.function.Function


/**
 * File Name: [JwtTokenUtil]
 * Created By nareshkatta on 25-Mar-2022
 * Updated by Naresh Katta at 8:10 PM 25-Mar-2022
 */
@Component
class JwtTokenUtil : Serializable {

    companion object {
        const val JWT_TOKEN_VALIDITY = (2 * 60).toLong()
        val map = mutableMapOf<String, Boolean>()
    }

    @Value("\${jwt.secret}")
    private val secret: String? = null

    fun getUsernameFromToken(token: String?): String {
        return getClaimFromToken(token!!, Function<Claims, String> { obj: Claims -> obj.subject })
    }

    fun getIssuedAtDateFromToken(token: String): Date {
        return getClaimFromToken(token, Function<Claims, Date> { obj: Claims -> obj.issuedAt })
    }

    fun getExpirationDateFromToken(token: String): Date {
        return getClaimFromToken(token, Function<Claims, Date> { obj: Claims -> obj.expiration })
    }


    private fun <T> getClaimFromToken(token: String, claimsResolver: Function<Claims, T>): T {
        val claims: Claims = getAllClaimsFromToken(token)
        return claimsResolver.apply(claims)
    }

    private fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody()
    }

    private fun isTokenExpired(token: String): Boolean {
        return try {
            val expiration = getExpirationDateFromToken(token)
            expiration.before(Date())
        } catch (e: ExpiredJwtException) {
            true
        }
    }

    private fun ignoreTokenExpiration(token: String): Boolean {
        // here you specify tokens, for that the expiration is ignored
        return token == ""
    }

    fun generateToken(userDetails: UserDetails): String {
        val claims: Map<String, Any> = HashMap()
        return doGenerateToken(claims, userDetails.username)
    }

    private fun doGenerateToken(claims: Map<String, Any>, subject: String): String {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(Date(System.currentTimeMillis()))
            .setExpiration(Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000)).signWith(
                SignatureAlgorithm.HS512, secret
            ).compact()
    }

    fun canTokenBeRefreshed(token: String): Boolean {
        return !isTokenExpired(token) || ignoreTokenExpiration(token)
    }

    fun blacklistToken(token: String) {
        clearExpired()
        map[token] = true
    }

    private fun clearExpired() {
        map.entries.removeIf { entry -> isTokenExpired(entry.key) }
    }

    fun validateToken(token: String, userDetails: UserDetails): Boolean {
        val username = getUsernameFromToken(token)
        return username == userDetails.username && !isTokenExpired(token) && !map.getOrDefault(token, false)
    }

}
