package com.kratosinnovationlabs.foodlens.config

import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.Serializable
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * File Name: [JwtAuthenticationEntryPoint]
 * Created By nareshkatta on 25-Mar-2022
 * Updated by Naresh Katta at 8:02 PM 25-Mar-2022
 */
@Component
class JwtAuthenticationEntryPoint : AuthenticationEntryPoint, Serializable {
    @Throws(IOException::class)
    override fun commence(
        request: HttpServletRequest?, response: HttpServletResponse,
        authException: AuthenticationException?
    ) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid or expired token")
    }

    companion object {
        private const val serialVersionUID = -7858869558953243875L
    }
}
