package com.kratosinnovationlabs.foodlens.config

import com.kratosinnovationlabs.foodlens.service.JwtUserDetailsService
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.UnsupportedJwtException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * File Name: [JwtRequestFilter]
 * Created By nareshkatta on 25-Mar-2022
 * Updated by Naresh Katta at 8:07 PM 25-Mar-2022
 */
@Component
class JwtRequestFilter(
    val jwtUserDetailsService: JwtUserDetailsService,
    val jwtTokenUtil: JwtTokenUtil
) : OncePerRequestFilter() {

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse?, chain: FilterChain) {
        logger.info(request.requestURI ?: "")
        val requestTokenHeader = request.getHeader("Authorization")
        var username: String? = null
        var jwtToken: String? = null
        // JWT Token is in the form "Bearer token". Remove Bearer word and get only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7)
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken)
            } catch (e: IllegalArgumentException) {
                println("Unable to get JWT Token\nSTART OF JWT\n$jwtToken\n")
                response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unable to get JWT Token")
                return
            } catch (e: ExpiredJwtException) {
                println("JWT Token has expired\nSTART OF JWT\n$jwtToken\n")
                response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, "JWT Token has expired")
                return
            } catch (e: UnsupportedJwtException) {
                println("Unsupported JWT\nSTART OF JWT\n$jwtToken\n")
                response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unsupported JWT")
                return
            } catch (e: MalformedJwtException) {
                println("Malformed JWT\nSTART OF JWT\n$jwtToken\n")
                response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Malformed JWT")
                return
            } catch (e: SignatureException) {
                println("JWT signature does not match\nHack alert?\nSTART OF JWT\n$jwtToken\nEND OF JWT")
                response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, "JWT signature does not match\nHack alert?")
                return
            }
        } else {
            logger.info("No Token")
        }

        //Once we get the token validate it.
        if (username != null && jwtToken != null && SecurityContextHolder.getContext().authentication == null) {
            val userDetails: UserDetails = jwtUserDetailsService.loadUserByUsername(username)

            // if token is valid configure Spring Security to manually set authentication
            if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
                val usernamePasswordAuthenticationToken = UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.authorities
                )
                usernamePasswordAuthenticationToken.details = WebAuthenticationDetailsSource().buildDetails(request)
                // After setting the Authentication in the context, we specify
                // that the current user is authenticated. So it passes the Spring Security Configurations successfully.
                SecurityContextHolder.getContext().authentication = usernamePasswordAuthenticationToken
                jwtTokenUtil.blacklistToken(jwtToken)
            }
        }
        chain.doFilter(request, response)
    }
}
