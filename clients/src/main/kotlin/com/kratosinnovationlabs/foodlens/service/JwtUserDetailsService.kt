package com.kratosinnovationlabs.foodlens.service

import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.*

/**
 * File Name: [JwtUserDetailsService]
 * Created By nareshkatta on 25-Mar-2022
 * Updated by Naresh Katta at 8:08 PM 25-Mar-2022
 */
@Service
class JwtUserDetailsService : UserDetailsService {
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        return if ("yufincordabc" == username) {
            User(
                username, "Ncs@9r5lrc",
                ArrayList()
            )
        } else {
            throw UsernameNotFoundException("User not found with username: $username")
        }
    }
}
