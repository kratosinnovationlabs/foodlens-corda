package com.kratosinnovationlabs.foodlens.webserver

import com.kratosinnovationlabs.foodlens.body.JwtRequest
import com.kratosinnovationlabs.foodlens.body.JwtResponse
import com.kratosinnovationlabs.foodlens.config.JwtTokenUtil
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.*

/**
 * File Name: [JwtAuthenticationController]
 * Created By nareshkatta on 25-Mar-2022
 * Updated by Naresh Katta at 8:32 PM 25-Mar-2022
 */
@RestController
@CrossOrigin
@ComponentScan(value = ["com.kratosinnovationlabs.foodlens.config", "com.kratosinnovationlabs.foodlens.service"])
class JwtAuthenticationController(
    val authenticationManager: AuthenticationManager,
    val jwtTokenUtil: JwtTokenUtil,
    val jwtInMemoryUserDetailsService: UserDetailsService
) {

    @RequestMapping(
        value = ["/authenticate"],
        method = [RequestMethod.POST],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    @Throws(Exception::class)
    fun createAuthenticationToken(@RequestBody authenticationRequest: JwtRequest): ResponseEntity<*> {
        authenticate(authenticationRequest.username, authenticationRequest.password)
        val userDetails = jwtInMemoryUserDetailsService
            .loadUserByUsername(authenticationRequest.username)
        val token: String = jwtTokenUtil.generateToken(userDetails)
        return ResponseEntity.ok<Any>(JwtResponse(token))
    }

    @Throws(Exception::class)
    private fun authenticate(username: String, password: String) {
        try {
            authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
        } catch (e: DisabledException) {
            throw Exception("USER_DISABLED", e)
        } catch (e: BadCredentialsException) {
            throw Exception("INVALID_CREDENTIALS", e)
        }
    }
}
