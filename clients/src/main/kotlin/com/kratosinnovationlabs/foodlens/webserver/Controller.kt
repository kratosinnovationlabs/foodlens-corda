package com.kratosinnovationlabs.foodlens.webserver

import com.google.gson.Gson
import com.kratosinnovationlabs.foodlens.accountsUtilities.CreateNewAccount
import com.kratosinnovationlabs.foodlens.flows.AddProductOperationFlow
import com.kratosinnovationlabs.foodlens.flows.CreateProductFlow
import com.kratosinnovationlabs.foodlens.models.Product
import com.kratosinnovationlabs.foodlens.models.ProductOperation
import com.kratosinnovationlabs.foodlens.schemas.ProductSchemaV1
import com.kratosinnovationlabs.foodlens.states.ProductOperationState
import com.kratosinnovationlabs.foodlens.states.ProductState
import net.corda.client.jackson.JacksonSupport
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.internal.toX500Name
import net.corda.core.messaging.startFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.node.NodeInfo
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x500.style.BCStyle
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/") // The paths for HTTP requests are relative to this base path.
class Controller(rpc: NodeRPCConnection) {

    private val proxy = rpc.proxy
    private val me = proxy.nodeInfo().legalIdentities.first().name

    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    fun X500Name.toDisplayString(): String = BCStyle.INSTANCE.toString(this)

    /** Helpers for filtering the network map cache. */
    private fun isNotary(nodeInfo: NodeInfo) = proxy.notaryIdentities().any { nodeInfo.isLegalIdentity(it) }

    private fun isMe(nodeInfo: NodeInfo) = nodeInfo.legalIdentities.first().name == me
    private fun isNetworkMap(nodeInfo: NodeInfo) = nodeInfo.legalIdentities.single().name.organisation == "Network Map Service"

    /**
     * Returns the node's name.
     */
    @GetMapping(value = ["me"], produces = [APPLICATION_JSON_VALUE])
    fun whoami() = mapOf("me" to me.toString())

    /**
     * Returns all parties registered with the [NetworkMapService]. These names can be used to look up identities
     * using the [IdentityService].
     */
    @GetMapping(value = ["peers"], produces = [APPLICATION_JSON_VALUE])
    fun getPeers(): Map<String, List<String>> {
        return mapOf("peers" to proxy.networkMapSnapshot()
                .filter { isNotary(it).not() && isMe(it).not() && isNetworkMap(it).not() }
                .map { it.legalIdentities.first().name.toX500Name().toDisplayString() })
    }

    @PostMapping(value = ["create_account/{acctName}"], produces = [APPLICATION_JSON_VALUE])
    fun createAccount(@PathVariable acctName: String): ResponseEntity<String> {
        return try {
            val result = proxy.startFlow(::CreateNewAccount, acctName).returnValue.get()
            val map = mutableMapOf<String, String>()
            map["name"] = result.name
            map["host"] = result.host.name.toX500Name().toDisplayString()
            map["identifier"] = result.identifier.id.toString()
            ResponseEntity.status(HttpStatus.CREATED).body(Gson().toJson(map))

        } catch (e: Exception) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.message)
        }
    }

    @PostMapping(value = ["add_product"], produces = [APPLICATION_JSON_VALUE])
    fun addProduct(@RequestBody product: Product, @RequestParam("creator") farmer: String, @RequestParam("admin") admin: String): ResponseEntity<String> {
        return try {
            val batchQueryExpression = builder { ProductSchemaV1.PersistentProductState::product_id.equal(product.product_id) }
            val batchCriteria = QueryCriteria.VaultCustomQueryCriteria(batchQueryExpression)
            val list = proxy.vaultQueryBy<ProductState>(
                    criteria = batchCriteria
            )
            val map = mutableMapOf<String, Any?>()
            if (list.states.isNotEmpty()) {
                val first = list.states.first()
                map["message"] = "Record already exists with given product id"
                map["txn_id"] = first.ref.txhash.toString()
                map["object"] = first.state.data.product
                ResponseEntity.status(HttpStatus.CONFLICT).body(Gson().toJson(map))
            } else {
                val flowResult = proxy.startFlow(::CreateProductFlow, farmer, admin, product).returnValue.get()
                val state = flowResult.stx.tx.outputs.find { it.data is ProductState }?.data as ProductState
                val result = flowResult.stx
                map["txn_id"] = result.id.toString()
                map["object"] = state.product
                ResponseEntity.status(HttpStatus.OK).body(Gson().toJson(map))
            }
        } catch (e: Exception) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Gson().toJson(mapOf("message" to e.message)))
        }
    }

    @PostMapping(value = ["add_operation"], produces = [APPLICATION_JSON_VALUE])
    fun addProductOperation(@RequestBody product_op: ProductOperation, @RequestParam("creator") member: String, @RequestParam("admin") admin: String): ResponseEntity<String> {
        return try {
            val flowResult = proxy.startFlow(::AddProductOperationFlow, member, admin, product_op).returnValue.get()
            val map = mutableMapOf<String, Any?>()
            val state = flowResult.stx.tx.outputs.find { it.data is ProductOperationState }?.data as ProductOperationState
            val result = flowResult.stx
            map["txn_id"] = result.id.toString()
            map["object"] = state.product_operation
            ResponseEntity.status(HttpStatus.OK).body(Gson().toJson(map))
        } catch (e: Exception) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Gson().toJson(mapOf("message" to e.message)))
        }
    }

    @GetMapping(value = ["hash/{hash}"], produces = [APPLICATION_JSON_VALUE])
    fun hashDetails(@PathVariable("hash") hash: String): ResponseEntity<String> {
        val list = proxy.vaultQueryBy<ContractState>(
                criteria = QueryCriteria.VaultQueryCriteria(
                        status = Vault.StateStatus.ALL,
                        stateRefs = (0..40).map { StateRef(SecureHash.parse(hash), it) }
                )
        ).states
        val map = mutableMapOf<String, Any>()
        map["message"] = "No records with hash : $hash found"
        val mapper = JacksonSupport.createNonRpcMapper()
        return if (list.isEmpty())
            ResponseEntity.status(HttpStatus.NOT_FOUND).body(Gson().toJson(map))
        else
            ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(list))
    }
}