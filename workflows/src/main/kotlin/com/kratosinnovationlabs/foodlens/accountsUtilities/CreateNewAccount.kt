package com.kratosinnovationlabs.foodlens.accountsUtilities

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.accountService
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.flows.StartableByService
import net.corda.core.utilities.getOrThrow


@StartableByRPC
@StartableByService
@InitiatingFlow
class CreateNewAccount(private val acctName: String) : FlowLogic<AccountInfo>() {

    @Suspendable
    override fun call(): AccountInfo {
        //Add a new account
        val newAccount = accountService.createAccount(name = acctName).toCompletableFuture().getOrThrow()
        val acct = newAccount.state.data
        return acct
    }
}



