package com.kratosinnovationlabs.foodlens.flows

import co.paralleluniverse.fibers.Suspendable
import com.kratosinnovationlabs.foodlens.FlowResult
import com.kratosinnovationlabs.foodlens.accountsUtilities.NewKeyForAccount
import com.kratosinnovationlabs.foodlens.contracts.ProductContract
import com.kratosinnovationlabs.foodlens.contracts.ProductOperationContract
import com.kratosinnovationlabs.foodlens.models.Product
import com.kratosinnovationlabs.foodlens.states.ProductOperationState
import com.kratosinnovationlabs.foodlens.states.ProductState
import com.r3.corda.lib.accounts.workflows.accountService
import net.corda.core.contracts.Command
import net.corda.core.contracts.TransactionState
import net.corda.core.flows.*
import net.corda.core.identity.AnonymousParty
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
@InitiatingFlow
class CreateProductFlow(val farmer: String, val admin: String, val product: Product) : FlowResultFlowLogic() {
    @Suspendable
    override fun call(): FlowResult {
        val myAccount = accountService.accountInfo(farmer).single().state.data
        val admin = accountService.accountInfo(admin).single().state.data
        val owningKey = subFlow(NewKeyForAccount(myAccount.identifier.id)).owningKey
        val adminKey = subFlow(NewKeyForAccount(admin.identifier.id)).owningKey
        val productState = ProductState(AnonymousParty(owningKey), AnonymousParty(owningKey), AnonymousParty(adminKey), product)
        val productOpState = ProductOperationState(AnonymousParty(owningKey), AnonymousParty(adminKey), product.toOp())

        val notary = serviceHub.networkMapCache.notaryIdentities.single() // METHOD 1

        val productTxnStt = TransactionState(productState, ProductContract.CONTRACT_ID, notary)
        val productOpTxnStt = TransactionState(productOpState, ProductOperationContract.CONTRACT_ID, notary)
        val transactionBuilder = TransactionBuilder(notary)

        transactionBuilder.withItems(
                productTxnStt,
                Command(ProductContract.Commands.Create(), productTxnStt.data.participants.map { it.owningKey })
        )

        transactionBuilder.withItems(
                productOpTxnStt,
                Command(ProductOperationContract.Commands.Add(), productOpTxnStt.data.participants.map { it.owningKey })
        )
        transactionBuilder.verify(serviceHub)
        val ptx = serviceHub.signInitialTransaction(transactionBuilder)
        val sessions = (productOpState.participants + productOpState.participants - ourIdentity).toSet().asSequence().map { initiateFlow(it) }.toList()
        val notarised = notarizeTxn(ptx, sessions)
        return FlowResult(notarised, ourIdentity)
    }
}


@InitiatedBy(CreateProductFlow::class)
class CreateProductFlowResponder(val flowSession: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {

        val signedTransactionFlow = object : SignTransactionFlow(flowSession) {
            override fun checkTransaction(stx: SignedTransaction) {
            }
        }

        val txWeJustSignedId = subFlow(signedTransactionFlow)

        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession, expectedTxId = txWeJustSignedId.id))
    }
}