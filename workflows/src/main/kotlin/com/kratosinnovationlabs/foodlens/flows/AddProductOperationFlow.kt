package com.kratosinnovationlabs.foodlens.flows

import co.paralleluniverse.fibers.Suspendable
import com.kratosinnovationlabs.foodlens.FlowResult
import com.kratosinnovationlabs.foodlens.accountsUtilities.NewKeyForAccount
import com.kratosinnovationlabs.foodlens.contracts.ProductOperationContract
import com.kratosinnovationlabs.foodlens.models.ProductOperation
import com.kratosinnovationlabs.foodlens.states.ProductOperationState
import com.r3.corda.lib.accounts.workflows.accountService
import net.corda.core.contracts.Command
import net.corda.core.contracts.TransactionState
import net.corda.core.flows.*
import net.corda.core.identity.AnonymousParty
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
@InitiatingFlow
class AddProductOperationFlow(val member: String, val admin: String, val product_operation: ProductOperation) : FlowResultFlowLogic() {
    @Suspendable
    override fun call(): FlowResult {
        val myAccount = accountService.accountInfo(member).single().state.data
        val admin = accountService.accountInfo(admin).single().state.data
        val owningKey = subFlow(NewKeyForAccount(myAccount.identifier.id)).owningKey
        val adminKey = subFlow(NewKeyForAccount(admin.identifier.id)).owningKey
        val productOpState = ProductOperationState(AnonymousParty(owningKey), AnonymousParty(adminKey), product_operation)
        val notary = serviceHub.networkMapCache.notaryIdentities.single() // METHOD 1
        val productOpTxnStt = TransactionState(productOpState, ProductOperationContract.CONTRACT_ID, notary)
        val transactionBuilder = TransactionBuilder(notary)
        transactionBuilder.withItems(
                productOpTxnStt,
                Command(ProductOperationContract.Commands.Add(), productOpTxnStt.data.participants.map { it.owningKey })
        )
        transactionBuilder.verify(serviceHub)
        val ptx = serviceHub.signInitialTransaction(transactionBuilder)
        val sessions = (productOpState.participants - ourIdentity).map { initiateFlow(it) }
        val notarised = notarizeTxn(ptx, sessions)
        return FlowResult(notarised, ourIdentity)
    }
}


@InitiatedBy(AddProductOperationFlow::class)
class AddProductOperationFlowResponder(val flowSession: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {

        val signedTransactionFlow = object : SignTransactionFlow(flowSession) {
            override fun checkTransaction(stx: SignedTransaction) {
            }
        }

        val txWeJustSignedId = subFlow(signedTransactionFlow)

        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession, expectedTxId = txWeJustSignedId.id))
    }
}