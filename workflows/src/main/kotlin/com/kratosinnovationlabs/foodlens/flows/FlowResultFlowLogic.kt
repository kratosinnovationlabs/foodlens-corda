package com.kratosinnovationlabs.foodlens.flows

import co.paralleluniverse.fibers.Suspendable
import com.kratosinnovationlabs.foodlens.FlowResult
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction

abstract class FlowResultFlowLogic : FlowLogic<FlowResult>() {

    @Suspendable
    protected fun notarizeTxn(
            ptx: SignedTransaction,
            sessions: List<FlowSession>
    ): SignedTransaction {
        println("================  Starting Collect Signature Flow  ================")
        val stx = subFlow(CollectSignaturesFlow(ptx, sessions))
        println("================    Collect Signature Flow ENDED   ================")
        val notarised = finaliseTx(stx, emptyList())
        println("================        Finality Flow ENDED        ================")
        return notarised
    }

    @Suspendable
    protected fun finaliseTx(
            tx: SignedTransaction,
            sessions: Collection<FlowSession>
    ): SignedTransaction {
        try {
            println("================     Starting Finality Flow    ================")
            return subFlow(FinalityFlow(tx, sessions))
        } catch (e: NotaryException) {
            throw RuntimeException("Unable to notarise issue", e)
        }
    }
}